CREATE TABLE if not exists 'shop' (
	'id'	INTEGER,
	'name'	TEXT,
	'latitude'	FLOAT,
	'longitude'	FLOAT,
        UNIQUE('name'),
	PRIMARY KEY('id')
);