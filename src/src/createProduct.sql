CREATE TABLE if not exists 'product' (
	'id'	INTEGER,
	'name'	TEXT,
	'price'	REAL,
	'value'	FLOAT,
	'unit'	TEXT,
	UNIQUE('name','price','value','unit'),
	PRIMARY KEY('id')
);