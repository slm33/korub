package korub;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static Stage stageMain;

    @Override
    public void start(Stage stage) throws Exception {
        Main.stageMain = stage;
        Parent root = FXMLLoader.load(getClass().getResource("Gui.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.resizableProperty().set(false);
        stage.setTitle("КоРуБъ");
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);

    }
}
