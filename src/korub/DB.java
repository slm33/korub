package korub;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DB {

    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;
    public static ObservableList<DBPojo> DBdata = FXCollections.observableArrayList();
    public static ObservableList<String> arrayShopName = FXCollections.observableArrayList();
    public static ObservableList<String> arrayProductName = FXCollections.observableArrayList();
    public static ObservableList<String> arrayUnit = FXCollections.observableArrayList();

// Подключение к БД
    public static void connectDB() throws SQLException, ClassNotFoundException {
        connection = null;
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:database.db");

//        System.out.println("База Подключена!");
    }

// Создание таблицы
    public static void createDB() throws SQLException {
        statement = connection.createStatement();
        statement.execute(fileToString("src/createDB.sql"));
        statement.execute(fileToString("src/createProduct.sql"));
        statement.execute(fileToString("src/createShop.sql"));
        statement.execute(fileToString("src/createUnit.sql"));
        statement.execute(fileToString("src/createSetting.sql"));
        statement.execute("INSERT or IGNORE INTO 'setting' ('name', 'value') VALUES ('useDB', 'true');");

//        System.out.println("Таблица создана или уже существует.");
    }

// Заполнение таблицы
    public static void writeDB() throws SQLException {
        statement.execute("INSERT or IGNORE INTO 'db' ('shop1', 'name1', 'price1', 'value1', 'percent1', 'unit1', 'shop2', 'name2', 'price2', 'value2', 'percent2', 'unit2') "
                + "VALUES (" + "'" + GuiController.shop1Static + "'" + ", "
                + "'" + GuiController.name1Static + "'" + ", "
                + GuiController.price1Static + ", "
                + GuiController.value1Static + ", "
                + GuiController.percent1Static + ", "
                + "'" + GuiController.unit1Static + "'" + ", "
                + "'" + GuiController.shop2Static + "'" + ", "
                + "'" + GuiController.name2Static + "'" + ", "
                + GuiController.price2Static + ", "
                + GuiController.value2Static + ", "
                + GuiController.percent2Static + ", "
                + "'" + GuiController.unit2Static + "'" + ");");
        statement.execute("INSERT or IGNORE INTO 'shop' ('name') VALUES (" + "'" + GuiController.shop1Static + "'" + ");");
        statement.execute("INSERT or IGNORE INTO 'shop' ('name') VALUES (" + "'" + GuiController.shop2Static + "'" + ");");
        statement.execute("INSERT or IGNORE INTO 'product' ('name', 'price', 'value', 'unit') "
                + "VALUES (" + "'" + GuiController.name1Static + "'" + ", "
                + GuiController.price1Static + ", "
                + GuiController.value1Static + ", "
                + "'" + GuiController.unit1Static + "'" + ");");
        statement.execute("INSERT or IGNORE INTO 'product' ('name', 'price', 'value', 'unit') "
                + "VALUES (" + "'" + GuiController.name2Static + "'" + ", "
                + GuiController.price2Static + ", "
                + GuiController.value2Static + ", "
                + "'" + GuiController.unit2Static + "'" + ");");
        statement.execute("INSERT or IGNORE INTO 'unit' ('unit') VALUES (" + "'" + GuiController.unit1Static + "'" + ");");
        statement.execute("INSERT or IGNORE INTO 'unit' ('unit') VALUES (" + "'" + GuiController.unit2Static + "'" + ");");

//        System.out.println("Таблица заполнена");
    }

// Вывод таблицы
    public static void readDB() throws SQLException {
        resultSet = statement.executeQuery("SELECT * FROM db");
        while (resultSet.next()) {
            DBdata.add(new DBPojo(resultSet.getInt("id"),
                    resultSet.getString("shop1"),
                    resultSet.getString("name1"),
                    resultSet.getFloat("price1"),
                    resultSet.getFloat("value1"),
                    resultSet.getInt("percent1"),
                    resultSet.getString("unit1"),
                    resultSet.getString("shop2"),
                    resultSet.getString("name2"),
                    resultSet.getFloat("price2"),
                    resultSet.getFloat("value2"),
                    resultSet.getInt("percent2"),
                    resultSet.getString("unit2")));
        }

//        System.out.println("Таблица выведена");
    }

    /**
     * Получение данных из БД для функции автодополнения текста
     *
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void readDBToStatic() throws ClassNotFoundException, SQLException {
        resultSet = statement.executeQuery("SELECT * FROM shop");
        while (resultSet.next()) {
            arrayShopName.add(resultSet.getString("name"));
        }
        resultSet = statement.executeQuery("SELECT * FROM product");
        while (resultSet.next()) {
            arrayProductName.add(resultSet.getString("name"));
        }
        resultSet = statement.executeQuery("SELECT * FROM unit");
        while (resultSet.next()) {
            arrayUnit.add(resultSet.getString("unit"));
        }

//        System.out.println("Таблица выведена");
    }

    /**
     * Удаление таблицы
     *
     * @param nameTable
     * @throws SQLException
     */
    public static void dropDB(String nameTable) throws SQLException {
        statement = connection.createStatement();
        statement.execute("drop table " + nameTable);

//        System.out.println("Таблица удалена.");
    }

    /**
     * Чтение строки таблицы
     * @param rowNumber
     * @throws SQLException
     */
    public static void readRow(String rowNumber) throws SQLException {
        resultSet = statement.executeQuery("SELECT shop1, name1, price1, value1, unit1, shop2, name2, price2, value2, unit2 FROM db where id=" + rowNumber);
        while (resultSet.next()) {
            GuiController.shop1Static = resultSet.getString("shop1");
            GuiController.name1Static = resultSet.getString("name1");
            GuiController.price1Static = resultSet.getString("price1");
            GuiController.value1Static = resultSet.getString("value1");
            GuiController.unit1Static = resultSet.getString("unit1");
            GuiController.shop2Static = resultSet.getString("shop2");
            GuiController.name2Static = resultSet.getString("name2");
            GuiController.price2Static = resultSet.getString("price2");
            GuiController.value2Static = resultSet.getString("value2");
            GuiController.unit2Static = resultSet.getString("unit2");
        }

//        System.out.println("Строка прочитана");
    }

    /**
     * Удаление строки таблицы
     * @param rowNumber
     * @throws SQLException 
     */
    public static void deleteRow(String rowNumber) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate("DELETE from db where ID=" + rowNumber + ";");

//        System.out.println("Строка удалена");
    }

// Закрытие соединения с БД
    public static void closeDB() throws SQLException {
        if (resultSet != null) {
            resultSet.close();
        }
        if (statement != null) {
            statement.close();
        }
        if (connection != null) {
            connection.close();
        }

//        System.out.println("Соединения закрыты");
    }

    /**
     * Преобразовывает файл в строку
     *
     * @param pathToFile Принимает путь типа String, например: src/fileName.txt
     * @return String или null при ошибке
     */
    static String fileToString(String pathToFile) {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        InputStream is = classLoader.getResourceAsStream(pathToFile);
        if (is != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            return reader.lines().collect(Collectors.joining(System.lineSeparator()));
        }
        return null;
    }

    /**
     * Обновление данных в ObservableList. Очистка → (DB.)connect → create → read → close
     */
    public static void refreshData() {
        DBdata.clear();
        try {
            connectDB();
            createDB();
            readDB();
            closeDB();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Запись данных в БД
     */
    public static void writeData() {
        try {
            DB.connectDB();
            DB.createDB();
            DB.writeDB();
            DB.closeDB();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void writeSetting(String setting, String value) throws SQLException {
        statement.execute("INSERT or REPLACE INTO 'setting' ('name', 'value') "
                + "VALUES (" + "'" + setting + "'" + ", '" + value + "'" + ");");
    }

    public static String readSetting(String setting) {
        try {
            resultSet = statement.executeQuery("SELECT value FROM setting where name = '" + setting + "'");
            String value = resultSet.getString("value");
            return value;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
