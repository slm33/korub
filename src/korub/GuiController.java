package korub;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.TextFields;

public class GuiController implements Initializable {

    static String sqlWrite;
    static String shop1Static = "";
    static String shop2Static = "";
    static String name1Static = "";
    static String name2Static = "";
    static String price1Static = "0";
    static String price2Static = "0";
    static String value1Static = "0";
    static String value2Static = "0";
    static String percent1Static = "%";
    static String percent2Static = "%";
    static String unit1Static = "";
    static String unit2Static = "";
    static String settingCheckUseDB = "";

    @FXML
    private Button button;
    @FXML
    private TextField priceField1;
    @FXML
    private TextField priceField2;
    @FXML
    private TextField valueField1;
    @FXML
    private TextField valueField2;
    @FXML
    private Label percentLabel1;
    @FXML
    private Label percentLabel2;
    @FXML
    private ImageView image1;
    @FXML
    private ImageView image2;
    @FXML
    private MenuItem close;
    @FXML
    private Button buttonShowDB;
    @FXML
    private TextField nameField1;
    @FXML
    private TextField nameField2;
    @FXML
    private TextField shopField1;
    @FXML
    private CheckBox checkUseDB;
    @FXML
    private MenuItem about;
    @FXML
    private TextField unitField1;
    @FXML
    private TextField unitField2;
    @FXML
    private TextField shopField2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
// Выделение всего текста при получении фокуса
//FIXME исправить этот кастыль, когда нормально будет работать .isFocused()
        priceField1.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue ov, Boolean t, Boolean t1) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (priceField1.isFocused() && !priceField1.getText().isEmpty()) {
                            priceField1.selectAll();
                        }
                    }
                });
            }
        });
        priceField2.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue ov, Boolean t, Boolean t1) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (priceField2.isFocused() && !priceField2.getText().isEmpty()) {
                            priceField2.selectAll();
                        }
                    }
                });
            }
        });
        valueField1.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue ov, Boolean t, Boolean t1) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (valueField1.isFocused() && !valueField1.getText().isEmpty()) {
                            valueField1.selectAll();
                        }
                    }
                });
            }
        });
        valueField2.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue ov, Boolean t, Boolean t1) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (valueField2.isFocused() && !valueField2.getText().isEmpty()) {
                            valueField2.selectAll();
                        }
                    }
                });
            }
        });
        shopField1.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue ov, Boolean t, Boolean t1) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (shopField1.isFocused() && !shopField1.getText().isEmpty()) {
                            shopField1.selectAll();
                        }
                    }
                });
            }
        });
        shopField2.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue ov, Boolean t, Boolean t1) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (shopField2.isFocused() && !shopField2.getText().isEmpty()) {
                            shopField2.selectAll();
                        }
                    }
                });
            }
        });
        nameField1.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue ov, Boolean t, Boolean t1) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (nameField1.isFocused() && !nameField1.getText().isEmpty()) {
                            nameField1.selectAll();
                        }
                    }
                });
            }
        });
        nameField2.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue ov, Boolean t, Boolean t1) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (nameField2.isFocused() && !nameField2.getText().isEmpty()) {
                            nameField2.selectAll();
                        }
                    }
                });
            }
        });
        unitField1.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue ov, Boolean t, Boolean t1) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (unitField1.isFocused() && !unitField1.getText().isEmpty()) {
                            unitField1.selectAll();
                        }
                    }
                });
            }
        });
        unitField2.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue ov, Boolean t, Boolean t1) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (unitField2.isFocused() && !unitField2.getText().isEmpty()) {
                            unitField2.selectAll();
                        }
                    }
                });
            }
        });

// Позволяет вводить только числа
        Pattern pattern = Pattern.compile("[0-9]*[,.]?[0-9]*");
        priceField1.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!pattern.matcher(newValue).matches()) {
                priceField1.setText(oldValue);
            }
        });
        priceField2.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!pattern.matcher(newValue).matches()) {
                priceField2.setText(oldValue);
            }
        });
        valueField1.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!pattern.matcher(newValue).matches()) {
                valueField1.setText(oldValue);
            }
        });
        valueField2.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!pattern.matcher(newValue).matches()) {
                valueField2.setText(oldValue);
            }
        });

        settingToStatic();
        shopField1.setText(shop1Static);
        shopField2.setText(shop2Static);
        priceField1.setText(price1Static);
        priceField2.setText(price2Static);
        valueField1.setText(value1Static);
        valueField2.setText(value2Static);
        nameField1.setText(name1Static);
        nameField2.setText(name2Static);
        unitField1.setText(unit1Static);
        unitField2.setText(unit2Static);
        percentLabel1.setText(percent1Static);
        percentLabel2.setText(percent2Static);
        textFieldAutoCompletion();
        
// Слушатель, который дублирует текст в другой unitField
        unitField1.textProperty().addListener((observable, oldValue, newValue) -> {
        unitField2.setText(unitField1.getText());
        });
        unitField2.textProperty().addListener((observable, oldValue, newValue) -> {
        unitField1.setText(unitField2.getText());
        });
    }

    private void textFieldAutoCompletion() {
        DB.arrayShopName.clear();
        DB.arrayProductName.clear();
        DB.arrayUnit.clear();
        try {
            DB.connectDB();
            DB.createDB();
            DB.readDBToStatic();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        removeDuplicateFromArray(DB.arrayShopName);
        removeDuplicateFromArray(DB.arrayProductName);
        removeDuplicateFromArray(DB.arrayUnit);
        TextFields.bindAutoCompletion(shopField1, DB.arrayShopName);
        TextFields.bindAutoCompletion(shopField2, DB.arrayShopName);
        TextFields.bindAutoCompletion(nameField1, DB.arrayProductName);
        TextFields.bindAutoCompletion(nameField2, DB.arrayProductName);
        TextFields.bindAutoCompletion(unitField1, DB.arrayUnit);
        TextFields.bindAutoCompletion(unitField2, DB.arrayUnit);
    }

    public static <E> void removeDuplicateFromArray(ObservableList<E> list) {
        HashSet<E> hashSet = new HashSet<>(list);
        list.clear();
        list.addAll(hashSet);
    }

    /**
     * Устанавливает текст из текстовых полей в статические переменные
     */
    private void textFieldToStatic() {
        shop1Static = shopField1.getText();
        shop2Static = shopField2.getText();
        name1Static = nameField1.getText();
        name2Static = nameField2.getText();
        price1Static = priceField1.getText();
        price2Static = priceField2.getText();
        value1Static = valueField1.getText();
        value2Static = valueField2.getText();
        unit1Static = unitField1.getText();
        unit2Static = unitField2.getText();
    }

    /**
     * Применяет настройки и записывает их в статические переменные
     */
    private void settingToStatic() {
        try {
            DB.connectDB();
            DB.createDB();
            settingCheckUseDB = DB.readSetting("useDB");
            DB.closeDB();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        checkUseDB.setSelected(Boolean.valueOf(settingCheckUseDB));
    }

    /**
     * Кнопка "Сравнить".
     */
    @FXML
    private void buttonAction(ActionEvent event) {
        percentLabel1.setText("");
        percentLabel2.setText("");
        textFieldToStatic();

// Проверка на пустые|нулевые поля и показ предупреждения
        if (price1Static.length() == 0 || price2Static.length() == 0
                || value1Static.length() == 0 || value2Static.length() == 0
                || price1Static.equals("0") || price2Static.equals("0")
                || value1Static.equals("0") || value2Static.equals("0")) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Обнаружены пустые или нулевые поля");
            alert.setContentText("Поля не должны быть пустыми или нулевыми.\n"
                    + "Попытка деления на ноль оскорбляет чувства\n"
                    + "компьютера.");
            alert.showAndWait();
            return;
        }
// Замена запятой на точку
        price1Static = price1Static.replace(",", ".");
        price2Static = price2Static.replace(",", ".");
        value1Static = value1Static.replace(",", ".");
        value2Static = value2Static.replace(",", ".");

        float price1 = Float.valueOf(price1Static);
        float price2 = Float.valueOf(price2Static);
        float value1 = Float.valueOf(value1Static);
        float value2 = Float.valueOf(value2Static);
// Расчёт соотношения (коэффициента)
        float ratio1 = price1 / value1;
        float ratio2 = price2 / value2;
        int percent1 = (int) (ratio2 / ratio1 * 100);
        int percent2 = (int) (ratio1 / ratio2 * 100);

        percent1Static = String.valueOf(percent1);
        percent2Static = String.valueOf(percent2);

        if (ratio1 < ratio2) {
            percentLabel1.setText(percent1 + "%");
            image1.setImage(new Image("src/ok.png"));
            image2.setImage(new Image("src/no.png"));
        } else if (ratio1 > ratio2) {
            percentLabel2.setText(percent2 + "%");
            image1.setImage(new Image("src/no.png"));
            image2.setImage(new Image("src/ok.png"));
        } else {
            percentLabel1.setText("=");
            percentLabel2.setText("=");
            image1.setImage(new Image("src/ok.png"));
            image2.setImage(new Image("src/ok.png"));
        }

// запись в БД
        if (checkUseDB.isSelected()) {
            DB.writeData();
        }
        textFieldAutoCompletion();
    }

    @FXML
    private void closeAction(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    private void buttonShowDB(ActionEvent event) throws IOException {
        textFieldToStatic();
        Parent root = FXMLLoader.load(getClass().getResource("DB.fxml"));
        Scene scene = new Scene(root);
        Stage stage = Main.stageMain;
        stage.hide();
        stage.setTitle("База Данных");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void aboutAction(ActionEvent event) throws IOException {
        textFieldToStatic();
        Parent root = FXMLLoader.load(getClass().getResource("About.fxml"));
        Scene scene = new Scene(root);
        Stage stage = Main.stageMain;
        stage.hide();
        stage.setTitle("О программе");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void checkUseDB(ActionEvent event) {
        settingCheckUseDB = String.valueOf(checkUseDB.isSelected());
        try {
            DB.connectDB();
            DB.createDB();
            DB.writeSetting("useDB", settingCheckUseDB);
            DB.closeDB();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
