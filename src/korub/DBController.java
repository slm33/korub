package korub;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 */
public class DBController implements Initializable {

    @FXML
    private TableView<DBPojo> tableView;
    @FXML
    private TableColumn<DBPojo, Integer> tableId;
    @FXML
    private TableColumn<DBPojo, String> tableName1;
    @FXML
    private TableColumn<DBPojo, Float> tablePrice1;
    @FXML
    private TableColumn<DBPojo, Float> tableValue1;
    @FXML
    private TableColumn<DBPojo, Integer> tablePercent1;
    @FXML
    private TableColumn<DBPojo, String> tableName2;
    @FXML
    private TableColumn<DBPojo, Float> tablePrice2;
    @FXML
    private TableColumn<DBPojo, Float> tableValue2;
    @FXML
    private TableColumn<DBPojo, Integer> tablePercent2;
    @FXML
    private Button buttonRefresh;
    @FXML
    private Button buttonDeleteRow;
    @FXML
    private Button buttonClear;
    @FXML
    private Button buttonUpdate;
    @FXML
    private TextField textFieldUpdate;
    @FXML
    private TextField textFieldDeleteRow;
    @FXML
    private Button buttonMainShow;
    @FXML
    private TableColumn<DBPojo, String> tableShop1;
    @FXML
    private TableColumn<DBPojo, String> tableUnit1;
    @FXML
    private TableColumn<DBPojo, String> tableShop2;
    @FXML
    private TableColumn<DBPojo, String> tableUnit2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
// Выделение всего текста при получении фокуса
//FIXME исправить этот кастыль, когда нормально будет работать .isFocused()
        textFieldUpdate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue ov, Boolean t, Boolean t1) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (textFieldUpdate.isFocused() && !textFieldUpdate.getText().isEmpty()) {
                            textFieldUpdate.selectAll();
                        }
                    }
                });
            }
        });
        textFieldDeleteRow.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue ov, Boolean t, Boolean t1) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (textFieldDeleteRow.isFocused() && !textFieldDeleteRow.getText().isEmpty()) {
                            textFieldDeleteRow.selectAll();
                        }
                    }
                });
            }
        });

// Позволяет вводить только числа
        Pattern pattern = Pattern.compile("[0-9]*");
        textFieldUpdate.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!pattern.matcher(newValue).matches()) {
                textFieldUpdate.setText(oldValue);
            }
        });
        textFieldDeleteRow.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!pattern.matcher(newValue).matches()) {
                textFieldDeleteRow.setText(oldValue);
            }
        });

        tableId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableShop1.setCellValueFactory(new PropertyValueFactory<>("shop1"));
        tableName1.setCellValueFactory(new PropertyValueFactory<>("name1"));
        tablePrice1.setCellValueFactory(new PropertyValueFactory<>("price1"));
        tableValue1.setCellValueFactory(new PropertyValueFactory<>("value1"));
        tablePercent1.setCellValueFactory(new PropertyValueFactory<>("percent1"));
        tableUnit1.setCellValueFactory(new PropertyValueFactory<>("unit1"));
        tableShop2.setCellValueFactory(new PropertyValueFactory<>("shop2"));
        tableName2.setCellValueFactory(new PropertyValueFactory<>("name2"));
        tablePrice2.setCellValueFactory(new PropertyValueFactory<>("price2"));
        tableValue2.setCellValueFactory(new PropertyValueFactory<>("value2"));
        tablePercent2.setCellValueFactory(new PropertyValueFactory<>("percent2"));
        tableUnit2.setCellValueFactory(new PropertyValueFactory<>("unit2"));

        DB.refreshData();
        tableView.setItems(DB.DBdata);
    }

    
    @FXML
    private void buttonRefresh(ActionEvent event) {
        DB.refreshData();
    }

    @FXML
    private void buttonUpdate(ActionEvent event) throws SQLException, ClassNotFoundException, IOException {
        String textField = textFieldUpdate.getText();
        if (textField.length() > 0) {
            DB.connectDB();
            DB.createDB();
            DB.readRow(textField);
            DB.deleteRow(textField);
            DB.closeDB();
            buttonMainShow(event);
        }
    }

    @FXML
    private void buttonDeleteRow(ActionEvent event) throws SQLException, ClassNotFoundException {
        String textField = textFieldDeleteRow.getText();
        if (textField.length() > 0) {
            DB.connectDB();
            DB.createDB();
            DB.deleteRow(textField);
            DB.closeDB();

            DB.refreshData();
        }
    }

    @FXML
    private void buttonClear(ActionEvent event) throws ClassNotFoundException, SQLException {
        DB.connectDB();
        DB.dropDB("db");
        DB.closeDB();

        DB.refreshData();
    }

    @FXML
    private void buttonMainShow(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Gui.fxml"));
        Scene scene = new Scene(root);
        Stage stage = Main.stageMain;
        stage.hide();
        stage.setTitle("КоРуБъ");
        stage.setScene(scene);
        stage.show();
    }
}
