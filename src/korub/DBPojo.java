package korub;

public class DBPojo {

    private int id;
    private String shop1;
    private String name1;
    private float price1;
    private float value1;
    private int percent1;
    private String unit1;
    private String shop2;
    private String name2;
    private float price2;
    private float value2;
    private int percent2;
    private String unit2;
    private float y;
    private float x;

    //
    DBPojo(int id, String shop1, String name1, float price1, float value1, int percent1, String unit1, String shop2, String name2, float price2, float value2, int percent2, String unit2) {
        this.id = id;
        this.shop1 = shop1;
        this.name1 = name1;
        this.price1 = price1;
        this.value1 = value1;
        this.percent1 = percent1;
        this.unit1 = unit1;
        this.shop2 = shop2;
        this.name2 = name2;
        this.value2 = value2;
        this.price2 = price2;
        this.percent2 = percent2;
        this.unit2 = unit2;
    }

    public int getId() {
        return id;
    }

    public String getShop1() {
        return shop1;
    }

    public String getName1() {
        return name1;
    }

    public float getPrice1() {
        return price1;
    }

    public float getValue1() {
        return value1;
    }

    public int getPercent1() {
        return percent1;
    }

    public String getUnit1() {
        return unit1;
    }

    public String getShop2() {
        return shop2;
    }

    public String getName2() {
        return name2;
    }

    public float getPrice2() {
        return price2;
    }

    public float getValue2() {
        return value2;
    }

    public int getPercent2() {
        return percent2;
    }

    public String getUnit2() {
        return unit2;
    }
}
